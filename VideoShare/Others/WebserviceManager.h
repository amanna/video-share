//
//  WebserviceManager.h
//  VideoShare
//
//  Created by Aditi Manna on 11/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^FetchCompletionHandler)(id object, NSError *error);
@interface WebserviceManager : NSObject
+(void)fetchRecentVideoOnCompletion:(NSString*)strVideoType withPage:(NSInteger)pageNo OnCompletion:(FetchCompletionHandler)handler;
+ (void)fetchVideoDataWithLink:(NSString*)strVideoType andQueueId:(NSString *)strQueueId OnCompletion:(FetchCompletionHandler)handler;
+ (void)callLogin:(NSString*)strUser andPass:(NSString *)strPass OnCompletion:(FetchCompletionHandler)handler;
+ (void)callFBLogin:(NSDictionary*)dictFb OnCompletion:(FetchCompletionHandler)handler;



//+ (void)fetchRecentPostOnCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchPostDetails:(NSString *)strPostId onCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchMenuDetails:(NSString*)strMenuId withPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchSearchDetails:(NSString *)strSearch onCompletiion:(FetchCompletionHandler)handler;
//+ (void)submitComment:(NSString *)strPostId  withText:(NSString*)strComment  onCompletion:(FetchCompletionHandler)handler;
//+ (void)registerRemoteNotificationWithDeviceId:(NSString *)deviceToken onCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchPostTypeDetails:(NSString *)strPostId withType:(NSString*)strType onCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchRecentPostPagewiseOnCompletion:(NSInteger)page onCompletion:(FetchCompletionHandler)handler;
//+ (NSString *)URLEncodeStringFromString:(NSString *)string;
//+ (void)callClearBadgeTokenServiceOncompletion:(FetchCompletionHandler)handler;
//+ (void)callMapService:(FetchCompletionHandler)handler;
//+ (void)fetchtagMenuDetails:(NSString*)strMenuId withPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler;
//+ (void)storeDeviceMeataDataToBackendOnCompletion:(FetchCompletionHandler)handler;
//+ (void)getAppSettingsDataOnCompletion:(FetchCompletionHandler)handler;
//+ (void)getWovaxSettingsDataOnCompletion:(FetchCompletionHandler)handler;
//+ (void)fetchRecentPostDictOfPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler ;
//+ (void)fetchNotificationOfDeviceId:(NSString *)deviceId OnCompletion:(FetchCompletionHandler)handler;
//+ (void)unsubscribePushNotificationOnCatergories:(NSString *)categories deviceId:(NSString *)deviceId onCompletion:(FetchCompletionHandler)handler ;
@end
