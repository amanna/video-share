//
//  UtilityManager.h
//  VideoShare
//
//  Created by Aditi Manna on 11/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilityManager : NSObject
+ (void)showAlertWithMessage:(NSString *)message;
+ (BOOL)isReachableToInternet;
+(NSString *)decodeHTMLCharacterEntities:(NSString*)str;
//+(CGFloat)getAspectHeightOfImage:(UIImage *)image ofWidth:(CGFloat)width;
+(BOOL)validateEmail: (NSString *) candidate;
+ (void)showAlertWithMessageTitle:(NSString *)message withTitle:(NSString*)strTitle;
@end
