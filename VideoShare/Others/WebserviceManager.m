//
//  WebserviceManager.m
//  VideoShare
//
//  Created by Aditi Manna on 11/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import "WebserviceManager.h"
#import "SSRestManager.h"
#import "Appconstants.h"
#import "VideoDetails.h"
#import "Videos.h"

//{"status":1,"error":"","result":[]}

@implementation WebserviceManager
+(void)fetchRecentVideoOnCompletion:(NSString*)strVideoType withPage:(NSInteger)pageNo OnCompletion:(FetchCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl;
    if([strVideoType isEqualToString:@"recent"]){
        menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_recentlycreated"];
    }else if ([strVideoType isEqualToString:@"popular"]){
        menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_mostpopularvideos"];
    }else{
        menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_highestratedviedeos"];
    }
    NSString *strPage = [NSString stringWithFormat:@"pagenumber=%d",pageNo];
   [restManager getJsonResponseFromBaseUrl:menuUrl query:strPage onCompletion:^(NSDictionary *json) {
        if (json) {
            NSMutableArray *vidFullList = [[NSMutableArray alloc] init];
            NSInteger status = [[json valueForKey:@"status"]integerValue];
            NSArray *vidList = [json valueForKey:@"result"];
                if(vidList.count > 0){
                [vidList enumerateObjectsUsingBlock:^(NSDictionary *vidDict, NSUInteger idx, BOOL *stop) {
                    Videos *vid= [[Videos alloc]initWithDict:vidDict];
                    [vidFullList addObject:vid];
                    
                }];
            }
            
            handler (vidFullList,nil);
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];
}
+ (void)fetchVideoDataWithLink:(NSString*)strVideoType andQueueId:(NSString *)strQueueId OnCompletion:(FetchCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl;
    if([strVideoType isEqualToString:@"recent"]){
        menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_recently_created_video_details"];
    }else if ([strVideoType isEqualToString:@"popular"]){
        menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_mostpopular_video_details"];
    }else{
        menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_highestrated_video_details"];
    }
    strQueueId = [NSString stringWithFormat:@"queueid=%@",strQueueId];
    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQueueId onCompletion:^(NSDictionary *json) {
        if (json) {
            NSMutableArray *vidFullList = [[NSMutableArray alloc] init];
            NSInteger status = [[json valueForKey:@"status"]integerValue];
            if(status==1){
                NSArray *vidList = [json valueForKey:@"result"];
                [vidList enumerateObjectsUsingBlock:^(NSDictionary *vidDict, NSUInteger idx, BOOL *stop) {
                    VideoDetails *vid= [[VideoDetails alloc]initWithDict:vidDict];
                    [vidFullList addObject:vid];
                    
                }];
                
                handler(vidFullList,nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
+ (void)callLogin:(NSString*)strUser andPass:(NSString *)strPass OnCompletion:(FetchCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl;
    menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,@"webservice_appuserlogin"];
    NSString *strQuery = [NSString stringWithFormat:@"username=%@&password=%@",strUser,strPass];
    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            NSInteger status = [[json valueForKey:@"status"]integerValue];
            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
            if([strStatus isEqualToString:@"1"]){
            //store user value in ios
            NSString *result = [json valueForKey:@"result"];
            NSString *uname = [result valueForKey:@"user_login"];
            [[NSUserDefaults standardUserDefaults] setObject:uname forKey:@"uname"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            }
            handler(strStatus,nil);
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
+ (void)callFBLogin:(NSDictionary*)dictFb OnCompletion:(FetchCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc] init];
    NSString *menuUrl;
    menuUrl = [NSString stringWithFormat:@"%@",kFbLoginUrl];
    NSString *strQuery = [NSString stringWithFormat:@"gender=%@&locale=%@&id=%@&updated_time=%@&last_name=%@&timezone=%@&email=%@&link=%@&verified=%@&first_name=%@",[dictFb valueForKey:@"gender"],[dictFb valueForKey:@"locale"],[dictFb valueForKey:@"id"],[dictFb valueForKey:@"updated_time"],[dictFb valueForKey:@"last_name"],[dictFb valueForKey:@"timezone"],[dictFb valueForKey:@"email"],[dictFb valueForKey:@"link"],[dictFb valueForKey:@"verified"],[dictFb valueForKey:@"first_name"]];
    [restManager getJsonResponseFromBaseUrl:menuUrl query:strQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            NSInteger status = [[json valueForKey:@"status"]integerValue];
            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
            if([strStatus isEqualToString:@"1"]){
                //store user value in ios
                NSString *result = [json valueForKey:@"result"];
                NSString *uname = [result valueForKey:@"user_login"];
                [[NSUserDefaults standardUserDefaults] setObject:uname forKey:@"uname"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            handler(strStatus,nil);
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
@end
