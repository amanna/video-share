//
//  Appconstants.h
//  VideoShare
//
//  Created by Aditi Manna on 11/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#ifndef VideoShare_Appconstants_h
#define VideoShare_Appconstants_h

#define kBaseURL @"http://dev.businessprodemo.com/qratetv/php/"
#define kFbLoginUrl @"http://dev.businessprodemo.com/qratetv/php/webservice_facebooklogin/"
#define kNoInternet @"No Internet! Please connect to a wifi network or activate cellular data internet."
#endif
