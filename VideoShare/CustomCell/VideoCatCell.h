//
//  VideoCell.h
//  VideoShare
//
//  Created by Aditi Manna on 17/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Videos.h"
@interface VideoCatCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *createBy;
@property (weak, nonatomic) IBOutlet UILabel *createDt;
@property (weak, nonatomic) IBOutlet UILabel *lblScore;

- (void)setUIWithDataSource:(Videos *)recentvideo withType:(NSString*)strListType;
@property (weak, nonatomic) IBOutlet UILabel *lblNoQueue;

@end
