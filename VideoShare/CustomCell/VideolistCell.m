//
//  VideolistCell.m
//  VideoShare
//
//  Created by Aditi Manna on 18/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import "VideolistCell.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "UtilityManager.h"
@implementation VideolistCell
- (void)setUIWithDataSource:(VideoDetails *)recentvideo{
    if(recentvideo.strVidthumb){
        //[UIImage imageNamed:@"blank.png"]
        NSURL *strUrl = [NSURL URLWithString:recentvideo.strVidthumb];
        [self.imgView setImageWithURL:strUrl placeholderImage:nil options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType){
            [self.imgView setImage:image];
        }];
        
    }
    self.lblTitle.text = recentvideo.strTitle;
    
    self.txtDesc.text = [UtilityManager decodeHTMLCharacterEntities:recentvideo.strContent];

}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
