//
//  VideoCell.m
//  VideoShare
//
//  Created by Aditi Manna on 17/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import "VideoCatCell.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "UtilityManager.h"
@implementation VideoCatCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setUIWithDataSource:(Videos *)recentvideo withType:(NSString*)strListType{
    if(recentvideo.strImageLink){
               //[UIImage imageNamed:@"blank.png"]
        NSURL *strUrl = [NSURL URLWithString:recentvideo.strImageLink];
        [self.imgView setImageWithURL:strUrl placeholderImage:nil options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType){
            [self.imgView setImage:image];
        }];

    }
    self.lblTitle.text = recentvideo.strTitle;
    self.createBy.text = recentvideo.strAuthorname;
    self.createDt.text = recentvideo.strCreateDate;
   // highest,popular,recent
    if([strListType isEqualToString:@"popular"] ){
        self.lblScore.text = [NSString stringWithFormat:@"Viewed: %@",recentvideo.strView];
    }else if ([strListType isEqualToString:@"highest"]){
        self.lblScore.text = [NSString stringWithFormat:@"Score: %@",recentvideo.strScore];
    }
}
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}
@end
