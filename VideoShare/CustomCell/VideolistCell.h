//
//  VideolistCell.h
//  VideoShare
//
//  Created by Aditi Manna on 18/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoDetails.h"
@interface VideolistCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

- (void)setUIWithDataSource:(VideoDetails *)recentvideo;
@property (weak, nonatomic) IBOutlet UITextView *txtDesc;
@end
