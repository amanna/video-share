//
//  VideoDetails.m
//  VideoShare
//
//  Created by Aditi Manna on 17/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import "VideoDetailsVC.h"
#import "VideoDetails.h"
#import "LoginViewcontroller.h"
#import "WebserviceManager.h"
#import "Reachability.h"
#import "Appconstants.h"
#import "UtilityManager.h"
#import "VideoCatCell.h"
#import <MediaPlayer/MediaPlayer.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Social/Social.h>
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
@interface VideoDetailsVC ()<UIWebViewDelegate>{
   // MPMoviePlayerController * moviePlayer;
}
@property(nonatomic)NSMutableArray *arrVidList;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIWebView *myWeb;
- (IBAction)fbshareAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtDesc;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

- (IBAction)btnLoginAction:(id)sender;

@end

@implementation VideoDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *uname = [prefs stringForKey:@"uname"];
    if([uname isEqualToString:@""]){
        [self.btnLogin setBackgroundImage:[UIImage imageNamed:@"login_button@3x.png"] forState:UIControlStateNormal];
        
    }
    
    self.lblTitle.text = self.vid.strTitle;
    self.txtDesc.text = self.vid.strContent;
    [self.indicator startAnimating];
    //[UtilityManager decodeHTMLCharacterEntities:self.vid.strContent];
    
        NSURL    *fileURL    =   [NSURL URLWithString:self.vid.strVidlink];
        
        float width = self.view.frame.size.width;
        float height = self.myWeb.frame.size.height;
        NSMutableString *html = [NSMutableString string];
        [html appendString:@"<html><head>"];
        
        [html appendString:@"</head><body style=\"margin:0\">"];
        [html appendFormat:@"<embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\"", fileURL];
         [html appendFormat:@"></embed>"];
       // [html appendFormat:@"width=\"%0.0f\" height=\"%0.0f\"></embed>", width, height];
        [html appendString:@"</body></html>"];
        [self.myWeb loadHTMLString:html
                           baseURL:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)fbshareAction:(id)sender {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        
        SLComposeViewController *controller=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        [controller addURL:[NSURL URLWithString:self.vid.strVidlink]];
        if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
            controller.view.hidden = YES;
        else
            controller.view.hidden = NO;
        [self presentViewController:controller animated:YES completion:Nil];
        }else{
        [UtilityManager showAlertWithMessageTitle:@"No Facebook Accounts" withTitle:@"There are no facebook accounts configured. You can add or create a Facebook account in Settings"];
    }

}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.indicator stopAnimating];
    self.indicator.hidden = YES;
}
- (IBAction)btnLoginAction:(id)sender {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *uname = [prefs stringForKey:@"uname"];
    if([uname isEqualToString:@""]){
        [self performSegueWithIdentifier:@"menuToLogin" sender:self];
        
    }else{
        [prefs setObject:@"" forKey:@"uname"];
        [prefs synchronize];
        for (UIViewController *controller in self.navigationController.viewControllers) {
            
            //Do not forget to import AnOldViewController.h
            if ([controller isKindOfClass:[LoginViewcontroller class]]) {
                
                [self.navigationController popToViewController:controller
                                                      animated:YES];
                break;
            }
        }
    }

}
@end
