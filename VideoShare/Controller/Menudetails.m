//
//  Menudetails.m
//  VideoShare
//
//  Created by Aditi Manna on 17/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import "Menudetails.h"
#import "WebserviceManager.h"
#import "Reachability.h"
#import "Appconstants.h"
#import "UtilityManager.h"
#import "VideoCatCell.h"
#import "VideoListVC.h"
#import "LoginViewcontroller.h"
@interface Menudetails ()
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic)NSMutableArray *arrVideoList;
- (IBAction)btnBackAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property(nonatomic)NSString *strQueue;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)loginClickedAction:(id)sender;

@property(nonatomic)BOOL isNewPageLoading;
@property(nonatomic)BOOL showNoMsg;
@property(nonatomic)NSInteger currPage;
@property(nonatomic)NSString *strQueueNmae;
@end

@implementation Menudetails

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *uname = [prefs stringForKey:@"uname"];
    if([uname isEqualToString:@""]){
        [self.btnLogin setBackgroundImage:[UIImage imageNamed:@"login_button@3x.png"] forState:UIControlStateNormal];
        
    }
    [self.tblView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.lblPopular.text = self.strListTypeNmae;
    self.lblHighest.text = self.strListTypeNmae;
    self.tblView.hidden = YES;
    self.indicator.hidden = NO;
    [self.indicator startAnimating];
    self.view.userInteractionEnabled = NO;
    self.currPage = 1;
    //parameter:highest,popular,recent
    if ([UtilityManager isReachableToInternet]) {
        [WebserviceManager fetchRecentVideoOnCompletion:self.strListType withPage:self.currPage OnCompletion:^(id object, NSError *error) {
            if(object){
                [self.indicator stopAnimating];
                self.indicator.hidden = YES;
                self.arrVideoList = object;
                [self.tblView reloadData];
                self.tblView.hidden = NO;
                self.isNewPageLoading = NO;
                self.currPage = self.currPage + 1;
                self.view.userInteractionEnabled = YES;
                }
        }];
    }else{
        [UtilityManager showAlertWithMessage:kNoInternet];
    }

    // Do any additional setup after loading the view.
}
#pragma mark scroll view delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    float offsetVal = (scrollView.contentOffset.y+scrollView.bounds.size.height);
    float contentSizeValue = (scrollView.contentSize.height);
    if ((int)offsetVal < (int)contentSizeValue){
        if(!self.isNewPageLoading){
            [self loadOneMorePage];
        }
        
    }
}
#pragma mark load more pages
- (void)loadOneMorePage {
    self.isNewPageLoading = YES;
    if ([UtilityManager isReachableToInternet]) {
        [WebserviceManager fetchRecentVideoOnCompletion:self.strListType withPage:self.currPage OnCompletion:^(NSArray *newList, NSError *error) {
            self.isNewPageLoading = NO;
            if(error){
                NSLog(@"Page=%d",self.currPage);
                [UtilityManager showAlertWithMessage:kNoInternet];
            }else if(newList.count > 0){
                NSLog(@"Page=%d",self.currPage);
                self.currPage= self.currPage +  1;
                [self.arrVideoList addObjectsFromArray:newList];
                [self.tblView reloadData];
            }else {
                self.isNewPageLoading = YES;
                NSLog(@"Page=%d",self.currPage);
            }
        }];
    }else{
        [UtilityManager showAlertWithMessage:kNoInternet];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if(self.arrVideoList.count==0){
        self.showNoMsg = YES;
        return 1;
    }else{
        self.showNoMsg = NO;
        return self.arrVideoList.count;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.showNoMsg==YES){
        return 44;
    }else{
         return 130;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    VideoCatCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(self.showNoMsg==YES){
        cell.lblNoQueue.hidden = NO;
    }else{
         cell.lblNoQueue.hidden = YES;
        Videos *vid = [self.arrVideoList objectAtIndex:indexPath.row];
        [cell setUIWithDataSource:vid withType:self.strListType];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
   
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Videos *vid = [self.arrVideoList objectAtIndex:indexPath.row];
    self.strQueue = vid.strQueueId;
    self.strQueueNmae = vid.strTitle;
   [self performSegueWithIdentifier:@"segueVidList" sender:self];
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segueVidList"]) {
        VideoListVC* menu = [segue destinationViewController];
        menu.strQueueName = self.strQueueNmae;
        menu.strQueueId = self.strQueue;
        menu.strListType = self.strListType;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)loginClickedAction:(id)sender {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *uname = [prefs stringForKey:@"uname"];
    if([uname isEqualToString:@""]){
        [self performSegueWithIdentifier:@"menuToLogin" sender:self];
        
    }else{
        
        [prefs setObject:@"" forKey:@"uname"];
        [prefs synchronize];
        for (UIViewController *controller in self.navigationController.viewControllers) {
            
            //Do not forget to import AnOldViewController.h
            if ([controller isKindOfClass:[LoginViewcontroller class]]) {
                
                [self.navigationController popToViewController:controller
                                                      animated:YES];
                break;
            }
        }
    }

}
@end
