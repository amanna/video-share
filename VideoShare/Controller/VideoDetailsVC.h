//
//  VideoDetails.h
//  VideoShare
//
//  Created by Aditi Manna on 17/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoDetails.h"
@interface VideoDetailsVC : UIViewController
@property(nonatomic)NSString *strQueue;
@property(nonatomic)NSString *strListType;
@property(nonatomic)VideoDetails *vid;
@end
