//
//  LoginViewcontroller.m
//  VideoShare
//
//  Created by Aditi Manna on 17/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import "LoginViewcontroller.h"
#import "WebserviceManager.h"
#import "UtilityManager.h"
#import "Appconstants.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@interface LoginViewcontroller ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPass;
- (IBAction)btnLoginClickedAction:(id)sender;

- (IBAction)btnRegisterClickedAction:(id)sender;
- (IBAction)btnBackAction:(id)sender;


@end

@implementation LoginViewcontroller

- (void)viewDidLoad {
    [super viewDidLoad];
    self.indicator.hidden = YES;
    NSString *dots =@"";
    for (int i=0; i<6; i++) {
        dots=[dots stringByAppendingString:@"•"];
        
    }
    self.txtPass.placeholder=dots;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma text field delegate method
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == self.txtUsername){
        [self.txtUsername resignFirstResponder];
        [self.txtPass becomeFirstResponder];
    }else{
        [self.txtPass resignFirstResponder];
    }
    
    return YES;
}
- (IBAction)btnLoginClickedAction:(id)sender {
    NSString *strUsername = self.txtUsername.text;
    NSString *strPass = self.txtPass.text;
    self.indicator.hidden = NO;
    [self.indicator startAnimating];
    if ([UtilityManager isReachableToInternet]) {
    [WebserviceManager callLogin:strUsername andPass:strPass OnCompletion:^(id object, NSError *error) {
        self.indicator.hidden = YES;
        [self.indicator stopAnimating];
               if(object){
                   NSString *str = object;
                   if([str isEqualToString:@"1"]){
                       [self performSegueWithIdentifier:@"segueMenu" sender:self];
                   }else{
                       [UtilityManager showAlertWithMessage:@"Invalid Username or Password"];
                   }
               }else{
                 [UtilityManager showAlertWithMessage:kNoInternet];
               }
    }];
    }else{
        [UtilityManager showAlertWithMessage:kNoInternet];
    }
    
}

- (IBAction)btnRegisterClickedAction:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
            NSLog(@"cancel");
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            NSLog(@"cancel123");
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
                if ([FBSDKAccessToken currentAccessToken]) {
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         if (!error) {
                            // [self performSegueWithIdentifier:@"segueMenu" sender:self];
                             NSDictionary *dict = (NSDictionary*)result;
                             NSLog(@"fetched user:%@", result);
                             //Login Webservice
                             
                             [WebserviceManager callFBLogin:dict OnCompletion:^(id object, NSError *error) {
                                 if(object){
                                     NSString *str = object;
                                     if([str isEqualToString:@"1"]){
                                         [self performSegueWithIdentifier:@"segueMenu" sender:self];
                                     }

                                 }else{
                                   [UtilityManager showAlertWithMessage:@"Some Problem occurs!!Please try again later"];  
                                 }
                             }];
                             
                             
                             
                             
                         }
                     }];
                }
        
            }
        }
    }];
    
}

- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
