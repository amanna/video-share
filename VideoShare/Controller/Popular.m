//
//  Popular.m
//  VideoShare
//
//  Created by Aditi Manna on 16/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import "Popular.h"
#import "WebserviceManager.h"
#import "Reachability.h"
#import "Appconstants.h"
#import "UtilityManager.h"
#import "VideoCatCell.h"
#import "VideoDetailsVC.h"
#import "VideoListVC.h"
#import "MenuViewController.h"
@interface Popular ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic)NSMutableArray *arrVideoList;
@property(nonatomic)BOOL isNewPageLoading;
@property(nonatomic)NSString *strQueue;
@property(nonatomic)NSInteger currPage;
@property(nonatomic)NSInteger pagelimit;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property(nonatomic)NSString *strQueueNmae;
- (IBAction)btnMenuClickedAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)btnLoginAction:(id)sender;

@end

@implementation Popular

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *uname = [prefs stringForKey:@"uname"];
    if(![uname isEqualToString:@""]){
        [self.btnLogin setBackgroundImage:[UIImage imageNamed:@"logout_button@3x.png"] forState:UIControlStateNormal];
        
    }
    
    self.arrVideoList = [[NSMutableArray alloc]init];
    self.navigationController.navigationBarHidden = YES;
    self.tblView.hidden = YES;
    self.view.userInteractionEnabled = NO;
    [self.indicator startAnimating];
    self.currPage = 1;
    self.isNewPageLoading = YES;
    //parameter:highest,popular,recent
    if ([UtilityManager isReachableToInternet]) {
       [WebserviceManager fetchRecentVideoOnCompletion:@"popular" withPage:self.currPage OnCompletion:^(id object, NSError *error) {
           if(object){
               self.currPage = self.currPage + 1;
               self.arrVideoList = object;
               self.tblView.hidden = NO;
               [self.tblView reloadData];
                self.view.userInteractionEnabled = YES;
               [self.indicator stopAnimating];
               self.indicator.hidden = YES;
               self.isNewPageLoading = NO;
           }
       }];
    }else{
         [UtilityManager showAlertWithMessage:kNoInternet];
    }

    // Do any additional setup after loading the view.
}
#pragma mark scroll view delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    float offsetVal = (scrollView.contentOffset.y+scrollView.bounds.size.height);
    float contentSizeValue = (scrollView.contentSize.height);
    if ((int)offsetVal < (int)contentSizeValue){
        if(!self.isNewPageLoading){
            [self loadOneMorePage];
        }
        
    }
}
#pragma mark load more pages
- (void)loadOneMorePage {
    self.isNewPageLoading = YES;
   if ([UtilityManager isReachableToInternet]) {
      [WebserviceManager fetchRecentVideoOnCompletion:@"popular" withPage:self.currPage OnCompletion:^(NSArray *newList, NSError *error) {
          self.isNewPageLoading = NO;
           if(error){
               NSLog(@"Page=%d",self.currPage);
               [UtilityManager showAlertWithMessage:kNoInternet];
           }else if(newList.count > 0){
                NSLog(@"Page=%d",self.currPage);
                self.currPage= self.currPage +  1;
               [self.arrVideoList addObjectsFromArray:newList];
               [self.tblView reloadData];
            }else {
               self.isNewPageLoading = YES;
                NSLog(@"Page=%d",self.currPage);
           }
       }];
    }else{
       [UtilityManager showAlertWithMessage:kNoInternet];
   }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return self.arrVideoList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    VideoCatCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    Videos *vid = [self.arrVideoList objectAtIndex:indexPath.row];
    [cell setUIWithDataSource:vid withType:@"popular"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Videos *vid = [self.arrVideoList objectAtIndex:indexPath.row];
    self.strQueue = vid.strQueueId;
    self.strQueueNmae = vid.strTitle;
    [self performSegueWithIdentifier:@"segueVidList" sender:self];
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segueVidList"]) {
        VideoListVC* menu = [segue destinationViewController];
        menu.strQueueName = self.strQueueNmae;
        menu.strQueueId = self.strQueue;
        menu.strListType = @"popular";
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnMenuClickedAction:(id)sender {
    [self performSegueWithIdentifier:@"segueMenu" sender:self];
}
- (IBAction)btnLoginAction:(id)sender {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *uname = [prefs stringForKey:@"uname"];
     if(![uname isEqualToString:@""]){
         [prefs setObject:@"" forKey:@"uname"];
         [prefs synchronize];
    }
    [self performSegueWithIdentifier:@"segueLogin" sender:self];
}
@end
