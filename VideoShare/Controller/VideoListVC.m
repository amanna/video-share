//
//  VideoListVC.m
//  VideoShare
//
//  Created by Aditi Manna on 18/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import "VideoListVC.h"
#import "VideolistCell.h"
#import "WebserviceManager.h"
#import "Reachability.h"
#import "Appconstants.h"
#import "UtilityManager.h"
#import "VideoDetails.h"
#import "VideoDetailsVC.h"
#import "LoginViewcontroller.h"
@interface VideoListVC ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property(nonatomic)NSMutableArray *arrVidList;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UILabel *lblListType;
@property(nonatomic)VideoDetails *vid;

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
- (IBAction)btnLoginAction:(id)sender;


@end

@implementation VideoListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *uname = [prefs stringForKey:@"uname"];
    if([uname isEqualToString:@""]){
        [self.btnLogin setBackgroundImage:[UIImage imageNamed:@"login_button@3x.png"] forState:UIControlStateNormal];
        
    }
    self.lblListType.text = self.strQueueName;
    self.arrVidList = [[NSMutableArray alloc]init];
    self.tblView.hidden = YES;
    self.view.userInteractionEnabled = NO;
    [self.indicator startAnimating];
    if ([UtilityManager isReachableToInternet]) {
        [WebserviceManager fetchVideoDataWithLink:self.strListType andQueueId:self.strQueueId OnCompletion:^(id object, NSError *error) {
            if(object){
                self.arrVidList = object;
                self.tblView.hidden = NO;
                [self.tblView reloadData];
                self.view.userInteractionEnabled = YES;
                [self.indicator stopAnimating];
                self.indicator.hidden = YES;
                
            }else{
                [UtilityManager showAlertWithMessage:kNoInternet];
            }
        }];
    }else{
        [UtilityManager showAlertWithMessage:kNoInternet];
    }

    // Do any additional setup after loading the view.
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    VideolistCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    VideoDetails *vid = [self.arrVidList objectAtIndex:indexPath.row];
    [cell setUIWithDataSource:vid];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.vid = [self.arrVidList objectAtIndex:indexPath.row];

   [self performSegueWithIdentifier:@"segueVidDetails" sender:self];
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segueVidDetails"]) {
         VideoDetailsVC* menu = [segue destinationViewController];
        menu.vid = self.vid;
        
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return self.arrVidList.count;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnLoginAction:(id)sender {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *uname = [prefs stringForKey:@"uname"];
    if([uname isEqualToString:@""]){
        [self performSegueWithIdentifier:@"menuToLogin" sender:self];
        
    }else{
        
        [prefs setObject:@"" forKey:@"uname"];
        [prefs synchronize];
        for (UIViewController *controller in self.navigationController.viewControllers) {
            
            //Do not forget to import AnOldViewController.h
            if ([controller isKindOfClass:[LoginViewcontroller class]]) {
                
                [self.navigationController popToViewController:controller
                                                      animated:YES];
                break;
            }
        }
    }

}
@end
