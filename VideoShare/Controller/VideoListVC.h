//
//  VideoListVC.h
//  VideoShare
//
//  Created by Aditi Manna on 18/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoListVC : UIViewController
@property(nonatomic)NSString *strQueueId;
@property(nonatomic)NSString *strQueueName;
@property(nonatomic)NSString *strListType;
@end
