//
//  MenuViewController.m
//  VideoShare
//
//  Created by Aditi Manna on 17/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import "MenuViewController.h"
#import "Menudetails.h"
#import "LoginViewcontroller.h"
@interface MenuViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic)NSMutableArray *arrMenu;
@property(nonatomic)NSMutableArray *arrMenu1;
@property(nonatomic)NSString *strListType;
@property(nonatomic)NSString *strListTypeNmae;
- (IBAction)btnLogoutAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *uname = [prefs stringForKey:@"uname"];
    if([uname isEqualToString:@""]){
        [self.btnLogin setBackgroundImage:[UIImage imageNamed:@"login_button@3x.png"] forState:UIControlStateNormal];
  
    }
    
    self.tblView.scrollEnabled = NO;
    [self.tblView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.arrMenu = [[NSMutableArray alloc]initWithObjects:@"Highest Rated",@"Recently Created",@"Most Popular", nil];//highest,popular,recent
    self.arrMenu1 = [[NSMutableArray alloc]initWithObjects:@"highest",@"recent",@"popular", nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    UILabel *aLabel = (UILabel *)[cell.contentView viewWithTag:400];
    aLabel.text = [self.arrMenu objectAtIndex:indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.strListType =[self.arrMenu1 objectAtIndex:indexPath.row];
    self.strListTypeNmae =[self.arrMenu objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"seguemenudetails" sender:self];
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"seguemenudetails"]) {
        Menudetails* menu = [segue destinationViewController];
        menu.strListType = self.strListType;
        menu.strListTypeNmae = self.strListTypeNmae;
        
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnLogoutAction:(id)sender {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *uname = [prefs stringForKey:@"uname"];
    if([uname isEqualToString:@""]){
        [self performSegueWithIdentifier:@"menuToLogin" sender:self];
        
    }else{
        
        [prefs setObject:@"" forKey:@"uname"];
        [prefs synchronize];
        for (UIViewController *controller in self.navigationController.viewControllers) {
            
            //Do not forget to import AnOldViewController.h
            if ([controller isKindOfClass:[LoginViewcontroller class]]) {
                
                [self.navigationController popToViewController:controller
                                                      animated:YES];
                break;
            }
        }
    }
}
@end
