//
//  Videos.m
//  VideoShare
//
//  Created by Aditi Manna on 17/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import "Videos.h"

@implementation Videos
- (id)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        self.strImageLink = [dict objectForKey:@"image"];
        self.strQueueId = [dict objectForKey:@"queueid"];
        self.strTitle = [dict objectForKey:@"title"];
        self.strAuthorname = [dict objectForKey:@"authorname"];
        self.strCreateDate = [dict objectForKey:@"createdate"];
        self.strScore = [dict objectForKey:@"score"];
        self.strView = [dict objectForKey:@"view"];
    }
    return self;
}

@end
