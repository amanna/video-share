//
//  VideoDetails.h
//  VideoShare
//
//  Created by Aditi Manna on 17/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoDetails : NSObject
//this is actually video

@property(nonatomic)NSString *strQueueId;
@property(nonatomic)NSString *strTitle;
@property(nonatomic)NSString *strContent;
@property(nonatomic)NSString *strVidlink;
@property(nonatomic)NSString *strVidthumb;
- (id)initWithDict:(NSDictionary *)dict;

@end
