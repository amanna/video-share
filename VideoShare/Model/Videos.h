//
//  Videos.h
//  VideoShare
//
//  Created by Aditi Manna on 17/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Videos : NSObject
@property(nonatomic)NSString *strImageLink;
@property(nonatomic)NSString *strQueueId;
@property(nonatomic)NSString *strTitle;
@property(nonatomic)NSString *strAuthorname;
@property(nonatomic)NSString *strCreateDate;
@property(nonatomic)NSString *strScore;
@property(nonatomic)NSString *strView;
- (id)initWithDict:(NSDictionary *)dict;
@end
