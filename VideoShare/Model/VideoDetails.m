//
//  VideoDetails.m
//  VideoShare
//
//  Created by Aditi Manna on 17/06/15.
//  Copyright (c) 2015 Limtex Infotech. All rights reserved.
//

#import "VideoDetails.h"

@implementation VideoDetails
- (id)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        self.strQueueId = [dict objectForKey:@"id"];
        self.strTitle = [dict objectForKey:@"post_title"];
        self.strContent = [dict objectForKey:@"post_content"];
        self.strVidlink = [dict objectForKey:@"video_link"];
        self.strVidthumb = [dict objectForKey:@"video_thumb"];
        
    }
    return self;
}

@end
